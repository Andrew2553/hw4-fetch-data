
// AJAX - це підхід у веб-розробці, який дозволяє взаємодіяти з сервером без необхідності перезавантаження сторінки. В основі цього підходу лежить використання JavaScript для відправки асинхронних запитів на сервер та отримання відповіді у форматі JSON, XML або інших.


const filmsContainer = document.getElementById('filmsContainer');

fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(response => response.json())
  .then(data => {
    data.forEach(film => {
      const filmElement = document.createElement('div');
      filmElement.innerHTML = `
        <h2>Епізод ${film.episodeId}: ${film.name}</h2>
        <p>Короткий зміст: ${film.openingCrawl}</p>
      `;
      
      film.characters.forEach(characterUrl => {
        fetch(characterUrl)
          .then(response => response.json())
          .then(character => {
            const characterElement = document.createElement('p');
            characterElement.textContent = `- Персонаж: ${character.name}`;
            filmElement.appendChild(characterElement);
          });
      });
      
      filmsContainer.appendChild(filmElement);
    });
  })
  .catch(error => {
    console.error('Виникла помилка:', error);
  });
